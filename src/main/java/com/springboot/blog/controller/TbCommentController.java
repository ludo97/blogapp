package com.springboot.blog.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.blog.DTO.ServiceResponseDTO;
import com.springboot.blog.exception.RecordNotFoundException;
import com.springboot.blog.model.TbCommentEntity;
import com.springboot.blog.model.TbCommentFileEntity;
import com.springboot.blog.service.TbCommentFileService;
import com.springboot.blog.service.TbCommentService;
import com.springboot.blog.service.TbUserService;

@RestController
@RequestMapping("/comment")
public class TbCommentController {
	@Autowired
	TbCommentService service;
	@Autowired
	Validator validator;
	@Autowired
	TbUserService service2;
	@Autowired
	TbCommentFileService service3;

	@GetMapping("/get")
	public ResponseEntity<ServiceResponseDTO> getArticleId(@RequestParam Integer articleId) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			if (articleId != null) {
				List<TbCommentEntity> content = service.findByArticleId(articleId);
				response.setStatus(true);
				response.setContent(content);
			} else {
				response.setMessage("nessun paramero inviato");
			}
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/edit")
	public ResponseEntity<ServiceResponseDTO> edit(HttpServletRequest request, @RequestBody TbCommentEntity comment)
			throws RecordNotFoundException {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			Integer id = service2.returnUserId(request);
			comment.setActive(true);
			comment.setUserId(id);
			comment.setDateCreation(new Date());
			Set<ConstraintViolation<TbCommentEntity>> t = validator.validate(comment);
			if (t.size() == 0) {
				TbCommentEntity updated = service.edit(comment);
				Map<String, Object> content = new HashMap<String, Object>();
				content.put("comment", updated);
				response.setContent((Object) content);
				response.setStatus(true);
			} else {
				response.setMessage(t.toString());
			}
		} catch (Exception ex) {
			response.setErrorMessage(ex.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@DeleteMapping("/remove")
	public ResponseEntity<ServiceResponseDTO> deleteById(@RequestParam Integer id) throws RecordNotFoundException {
		ServiceResponseDTO response = new ServiceResponseDTO();
		String msg = "";
		try {
			if (id <= 0) {
				msg = "Id deve essere maggiore di 0";
			} else {
				service.deleteById(id);
				List<TbCommentFileEntity> commentFileId = service3.findByCommentId2(id);
				if (!commentFileId.isEmpty()) {
					for (Integer i = 0; i < commentFileId.size(); i++) {
						service3.deleteById(i);
					}
				}
				response.setStatus(true);
				msg = "l'oggetto con id " + id + " è stato eliminato";

			}
		} catch (Exception ex) {
			response.setErrorMessage(ex.getMessage());
		}
		response.setMessage(msg);
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

}
