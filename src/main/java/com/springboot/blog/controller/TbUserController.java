package com.springboot.blog.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.WebUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.springboot.blog.DTO.ServiceResponseDTO;
import com.springboot.blog.DTO.UserRegDTO;
import com.springboot.blog.exception.RecordNotFoundException;
import com.springboot.blog.model.TbUserEntity;
import com.springboot.blog.service.TbUserService;
import com.springboot.blog.utility.RestClient;

@RestController
@RequestMapping("/user")
public class TbUserController {
	@Autowired
	TbUserService service;
	@Autowired
	Validator validator;
	@Autowired
	RestClient rc;

	@GetMapping("/get")
	public ResponseEntity<ServiceResponseDTO> getById(@RequestParam Integer id) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			if (id != null) {

				response.setStatus(true);
				TbUserEntity content = service.findById(id);
				response.setContent(content);
			} else {
				response.setMessageCode("parametro non valido");
			}
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/registration")
	public ResponseEntity<ServiceResponseDTO> edit(@RequestBody UserRegDTO user) throws RecordNotFoundException {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {

			Set<ConstraintViolation<UserRegDTO>> t = validator.validate(user);
			if (t.size() == 0) {

				Map<String, String> data = new HashMap<String, String>();
				String json = rc.post("http://authrest.dot-env.com/it/preauth", data);
				ObjectMapper mapper = new ObjectMapper();
				Map<String, String> map = mapper.readValue(json, Map.class);
				Object serviceStatus = map.get("serviceStatus");

				if ((boolean) serviceStatus) {

					Object content = map.get("content");
					Map<String, String> sysPreauth = (Map<String, String>) ((HashMap<String, Object>) content)
							.get("sysPreauth");
					String preauthUUID = sysPreauth.get("preauthUUID");

					data.put("preauthUUID", preauthUUID);
					data.put("appUUID", "3eaa6d1e-78c1-11e9-82e3-02003335eb48");
					data.put("userTypeUUID", "a7d2a357-7008-11e9-acd8-02003335eb48");
					data.put("username", user.getUsername());
					data.put("password", user.getPassword());
					data.put("mail", user.getEmail());
					data.put("linkIfExist", "true"); // fare il controllo per vedere se esiste gia l' utente
					String Response2 = rc.post("http://authrest.dot-env.com/it/register", data);
					Object obj2 = JSONValue.parse(Response2);
					JSONObject json2 = (JSONObject) obj2;
					Object serviceStatus2 = json2.get("serviceStatus");
					if ((Boolean) serviceStatus2) {
						Map<String, Object> content2 = (Map<String, Object>) json2.get("content");
						Map<String, String> user2 = (Map<String, String>) content2.get("user");
						String UUID = user2.get("userUUID");
						TbUserEntity updated = service.save(user, UUID);
						Map<String, Object> content3 = new HashMap<String, Object>();
						content3.put("user", updated);
						response.setContent((Object) content3);
						response.setStatus(true);
					} else {
						response.setMessage("registrazione NON andata a buon fine");
					}
				} else {
					response.setMessage("registrazione NON andata a buon fine");
				}
			} else {
				response.setMessage(t.toString());
			}
		} catch (Exception ex) {
			response.setErrorMessage(ex.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@DeleteMapping("/remove")
	public ResponseEntity<ServiceResponseDTO> deleteById(@RequestParam Integer id) throws RecordNotFoundException {
		ServiceResponseDTO response = new ServiceResponseDTO();
		String msg = "";
		try {
			if (id <= 0) {
				msg = "Id deve essere maggiore di 0";
			} else {
				service.deleteById(id);
				response.setStatus(true);
				msg = "l'oggetto con id " + id + " è stato eliminato";

			}
		} catch (Exception ex) {
			response.setErrorMessage(ex.getMessage());
		}
		response.setMessage(msg);
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/login")
	public ResponseEntity<ServiceResponseDTO> getIn(HttpServletRequest request, @RequestBody Map<String, String> userOb,
			HttpServletResponse aresponse) throws IOException {
		ServiceResponseDTO Sresponse = new ServiceResponseDTO();

		String username = userOb.get("username");
		String password = userOb.get("password");
		if (username != null || password != null) {

			Map<String, String> data = new HashMap<String, String>();
			data.put("username", username);
			data.put("password", password);
			data.put("appCodeName", "appCodeName");
			data.put("appName", "appName");
			data.put("appVersion", "appVersion");
			data.put("language", "language");
			data.put("platform", "platform");
			data.put("userAgent", "userAgent");
			data.put("buildId", "buildId");
			data.put("stayLogged", "true");
			data.put("mobile", "false");
			data.put("mobilePlatform", "mobilePlatform");
			data.put("serialNumber", "serialNumber");
			data.put("appUUID", "3eaa6d1e-78c1-11e9-82e3-02003335eb48");

			String response = rc.post("http://authrest.dot-env.com/it/login", data);

			Object obj = JSONValue.parse(response);
			JSONObject json = (JSONObject) obj;
			Object serviceStatus = json.get("serviceStatus");
			// userNamePasswordWrong
			if ((Boolean) serviceStatus) {

				Map<String, Object> content = (Map<String, Object>) json.get("content");
				Map<String, String> user = (Map<String, String>) content.get("user");
				Map<String, String> session = (Map<String, String>) content.get("session");
				Map<String, String> app = (Map<String, String>) content.get("app");
				String userUUID = user.get("userUUID");
				String userSessionUUID = session.get("userSessionUUID");
				String appUUID = app.get("appUUID");
				Cookie userUUID2 = new Cookie("userUUID", userUUID);
				Cookie appUUID2 = new Cookie("appUUID", appUUID);
				Cookie userSessionUUID2 = new Cookie("userSessionUUID", userSessionUUID);
				userUUID2.setPath("/");
				appUUID2.setPath("/");
				userSessionUUID2.setPath("/");
				aresponse.addCookie(userUUID2);
				aresponse.addCookie(appUUID2);
				aresponse.addCookie(userSessionUUID2);
				request.getSession().setAttribute("utente", content);
				request.getSession().setAttribute("username", username);

				Sresponse.setContent(userUUID);
				Sresponse.setStatus(true);
			} else {
				Sresponse.setErrorMessage("serviceStatus false");
			}
		} else {
			Sresponse.setErrorMessage("parametri inviati non validi");
		}

		return new ResponseEntity<ServiceResponseDTO>(Sresponse, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/logout")
	public ResponseEntity<ServiceResponseDTO> getOut(HttpServletRequest request) throws IOException {
		ServiceResponseDTO Sresponse = new ServiceResponseDTO();

		if (request != null) {

			String userUUID2 = WebUtils.getCookie(request, "userUUID").getValue();
			String appUUID2 = WebUtils.getCookie(request, "appUUID").getValue();
			String userSessionUUID2 = WebUtils.getCookie(request, "userSessionUUID").getValue();

			Map<String, String> data = new HashMap<String, String>();
			data.put("logAppUUID", appUUID2);
			data.put("logUserUUID", userUUID2);
			data.put("logUserSessionUUID", userSessionUUID2);

			String response = rc.post("http://authrest.dot-env.com/it/logout", data);

			Object obj = JSONValue.parse(response);
			JSONObject json = (JSONObject) obj;
			Object serviceStatus = json.get("serviceStatus");
			if ((Boolean) serviceStatus) {
				request.removeAttribute("utente");
				request.removeAttribute("username");
				request.getSession().invalidate();
				Map<String, Object> content3 = new HashMap<String, Object>();

				WebUtils.getCookie(request, "userUUID").setMaxAge(0);
				WebUtils.getCookie(request, "appUUID").setMaxAge(0);
				WebUtils.getCookie(request, "userSessionUUID").setMaxAge(0);

				content3.put("serviceStatus", serviceStatus);
				Sresponse.setContent(content3);
				Sresponse.setMessage("logout andato a buon fine");
				Sresponse.setStatus(true);
			}
		} else {
			Sresponse.setErrorMessage("parametri inviati non validi");
		}

		return new ResponseEntity<ServiceResponseDTO>(Sresponse, new HttpHeaders(), HttpStatus.OK);
	}

}
