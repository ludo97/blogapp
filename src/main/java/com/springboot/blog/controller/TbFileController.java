package com.springboot.blog.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.blog.DTO.FileParamDTO;
import com.springboot.blog.DTO.ServiceResponseDTO;
import com.springboot.blog.exception.RecordNotFoundException;
import com.springboot.blog.model.TbFileEntity;
import com.springboot.blog.model.TbStorageEntity;
import com.springboot.blog.service.TbFileService;
import com.springboot.blog.service.TbUserService;

@RestController
@RequestMapping("/file")
public class TbFileController {
	@Autowired
	TbFileService service;
	@Autowired
	Validator validator;
	@Autowired
	TbUserService service2;

	@GetMapping("/get")
	public ResponseEntity<ServiceResponseDTO> getSearch(@RequestParam String tag, String description, String fileName) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {

			FileParamDTO fileDTO = new FileParamDTO();
			fileDTO.setDescription(description);
			fileDTO.setFileName(fileName);
			fileDTO.setTag(tag);
			Set<ConstraintViolation<FileParamDTO>> t = validator.validate(fileDTO);
			if (t.size() == 0) {
				List<TbFileEntity> content = service.findForSearch(tag, description, fileName);
				response.setStatus(true);
				response.setContent(content);
			} else {
				response.setMessage("parametro non valido");
			}
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/getById")
	public ResponseEntity<ServiceResponseDTO> getById(@RequestParam Integer fileId) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			if (fileId != null) {

				response.setStatus(true);
				TbFileEntity content = service.findById(fileId);
				response.setContent(content);
			} else {
				response.setMessage("parametro inviato non valido");
			}
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/edit")
	public ResponseEntity<ServiceResponseDTO> edit(HttpServletRequest request, @RequestBody Map<String, Object> ent,
			TbFileEntity file, TbStorageEntity storage) throws RecordNotFoundException, IOException {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			Integer id = service2.returnUserId(request);
			if ((Integer) ent.get("fileId") != null) {
				file.setFileId((Integer) ent.get("fileId"));
			}
			file.setActive(true);
			file.setFileName((String) ent.get("fileName"));
			file.setTag((String) ent.get("tag"));
			file.setDescription((String) ent.get("description"));
			storage.setImmagine((String) ent.get("immagine"));

			file.setUserId(id);
			file.setDateUpload(new Date());
			Set<ConstraintViolation<TbFileEntity>> t = validator.validate(file);
			Set<ConstraintViolation<TbStorageEntity>> t2 = validator.validate(storage);
			if (t.size() == 0 || t2.size() == 0) {
				TbFileEntity updated = service.edit(file);
				TbStorageEntity updated2 = service.edit2(storage, updated);
				Map<String, Object> content = new HashMap<String, Object>();
				content.put("file", updated);
				content.put("storage", updated2);
				response.setContent((Object) content);
				response.setStatus(true);
			} else {
				response.setMessage(t.toString());
			}
		} catch (Exception ex) {
			response.setErrorMessage(ex.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@DeleteMapping("/remove")
	public ResponseEntity<ServiceResponseDTO> deleteById(@RequestParam Integer id) throws RecordNotFoundException {
		ServiceResponseDTO response = new ServiceResponseDTO();
		String msg = "";
		try {
			if (id <= 0) {
				msg = "Id deve essere maggiore di 0";
			} else {
				service.deleteById(id);
				Integer imgId = service.findByFileId2(id);
				service.deleteById2(imgId);
				response.setStatus(true);
				msg = "l'oggetto con id " + id + " è stato eliminato";

			}
		} catch (Exception ex) {
			response.setErrorMessage(ex.getMessage());
		}
		response.setMessage(msg);
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

}
