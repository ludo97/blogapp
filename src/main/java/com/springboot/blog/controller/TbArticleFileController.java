package com.springboot.blog.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.blog.DTO.ServiceResponseDTO;
import com.springboot.blog.exception.RecordNotFoundException;
import com.springboot.blog.model.TbArticleFileEntity;
import com.springboot.blog.service.TbArticleFileService;

@RestController
@RequestMapping("/article-file")
public class TbArticleFileController {
	@Autowired
	TbArticleFileService service;
	@Autowired
	Validator validator;

	@GetMapping("/get")
	public ResponseEntity<ServiceResponseDTO> getArticleId(@RequestParam Integer articleId) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			if (articleId != null) {
				List<TbArticleFileEntity> content = service.findByArticleId(articleId);
				response.setStatus(true);
				response.setContent(content);
			} else {
				response.setMessage("nessun paramero inviato");
			}
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/edit")
	public ResponseEntity<ServiceResponseDTO> edit(@RequestBody TbArticleFileEntity articleFile)
			throws RecordNotFoundException {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {

			articleFile.setDateCreation(new Date());
			articleFile.setActive(true);
			Set<ConstraintViolation<TbArticleFileEntity>> t = validator.validate(articleFile);
			if (t.size() == 0) {
				TbArticleFileEntity updated = service.edit(articleFile);
				Map<String, Object> content = new HashMap<String, Object>();
				content.put("articleFile", updated);
				response.setContent((Object) content);
				response.setStatus(true);
			} else {
				response.setMessage(t.toString());
			}
		} catch (Exception ex) {
			response.setErrorMessage(ex.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

}
