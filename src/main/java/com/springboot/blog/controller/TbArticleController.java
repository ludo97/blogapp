package com.springboot.blog.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.blog.DTO.ArticleParamDTO;
import com.springboot.blog.DTO.ServiceResponseDTO;
import com.springboot.blog.exception.RecordNotFoundException;
import com.springboot.blog.model.TbArticleEntity;
import com.springboot.blog.model.TbArticleFileEntity;
import com.springboot.blog.service.TbArticleFileService;
import com.springboot.blog.service.TbArticleService;
import com.springboot.blog.service.TbUserService;

@RestController
@RequestMapping("/article")
public class TbArticleController {
	@Autowired
	TbArticleService service;
	@Autowired
	Validator validator;
	@Autowired
	TbUserService service2;

	@Autowired
	TbArticleFileService service3;

	@GetMapping("/get")
	public ResponseEntity<ServiceResponseDTO> getSearch(@RequestParam String tag, String text, String title) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			ArticleParamDTO articleDTO = new ArticleParamDTO();
			articleDTO.setTag(tag);
			articleDTO.setText(text);
			articleDTO.setTitle(title);
			Set<ConstraintViolation<ArticleParamDTO>> t = validator.validate(articleDTO);
			if (t.size() == 0) {
				List<TbArticleEntity> content = service.findForSearch(tag, text, title);
				response.setStatus(true);
				response.setContent(content);
			} else {
				response.setMessage("nessun parametro inviato");
			}
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/edit")
	public ResponseEntity<ServiceResponseDTO> edit(HttpServletRequest request, @RequestBody TbArticleEntity article)
			throws RecordNotFoundException {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			Integer id = service2.returnUserId(request);
			article.setActive(true);
			article.setUserId(id);
			article.setDateCreation(new Date());
			Set<ConstraintViolation<TbArticleEntity>> t = validator.validate(article);
			if (t.size() == 0) {
				TbArticleEntity updated = service.edit(article);
				Map<String, Object> content = new HashMap<String, Object>();
				content.put("article", updated);
				response.setContent((Object) content);
				response.setStatus(true);
			} else {
				response.setMessage(t.toString());
			}
		} catch (Exception ex) {
			response.setErrorMessage(ex.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@DeleteMapping("/remove")
	public ResponseEntity<ServiceResponseDTO> deleteById(@RequestParam Integer id) throws RecordNotFoundException {
		ServiceResponseDTO response = new ServiceResponseDTO();
		String msg = "";
		try {
			if (id <= 0) {
				msg = "Id deve essere maggiore di 0";
			} else {
				service.deleteById(id);
				List<TbArticleFileEntity> articleFileId = service3.findByArticleId2(id);
				if (!articleFileId.isEmpty()) {
					for (Integer i = 0; i < articleFileId.size(); i++) {
						service3.deleteById(i);
					}
				}
				response.setStatus(true);
				msg = "l'oggetto con id " + id + " è stato eliminato";

			}
		} catch (Exception ex) {
			response.setErrorMessage(ex.getMessage());
		}
		response.setMessage(msg);
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

}
