package com.springboot.blog.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.blog.DTO.ServiceResponseDTO;
import com.springboot.blog.exception.RecordNotFoundException;
import com.springboot.blog.model.TbCommentFileEntity;
import com.springboot.blog.service.TbCommentFileService;

@RestController
@RequestMapping("/comment-file")
public class TbCommentFileController {
	@Autowired
	TbCommentFileService service;
	@Autowired
	Validator validator;

	@GetMapping("/get")
	public ResponseEntity<ServiceResponseDTO> getCommentId(@RequestParam Integer commentId) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			if (commentId != null) {
				List<TbCommentFileEntity> content = service.findByCommentId(commentId);
				response.setStatus(true);
				response.setContent(content);
			} else {
				response.setMessage("nessun paramero inviato");
			}
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/edit")
	public ResponseEntity<ServiceResponseDTO> edit(@RequestBody TbCommentFileEntity commentFile)
			throws RecordNotFoundException {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			commentFile.setActive(true);
			commentFile.setDateCreation(new Date());
			Set<ConstraintViolation<TbCommentFileEntity>> t = validator.validate(commentFile);
			if (t.size() == 0) {
				TbCommentFileEntity updated = service.edit(commentFile);
				Map<String, Object> content = new HashMap<String, Object>();
				content.put("commentFile", updated);
				response.setContent((Object) content);
				response.setStatus(true);
			} else {
				response.setMessage(t.toString());
			}
		} catch (Exception ex) {
			response.setErrorMessage(ex.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

}
