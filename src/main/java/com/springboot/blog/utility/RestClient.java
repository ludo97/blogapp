package com.springboot.blog.utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RestClient {
	@Autowired
	public RestClient() {}

	
	public String post(String endPoint, Map<String, String> data) throws IOException {
		return this.sendData(endPoint, "POST", data);
	}

	public String get(String endPoint, Map<String, String> data) throws IOException {
		return this.sendData(endPoint, "GET", data);
	}

	private String sendData(String endPoint, String method, Map<String, String> data) throws IOException {
		URL url = new URL(endPoint);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod(method);
		con.setInstanceFollowRedirects(true);
		StringBuffer payload = new StringBuffer();
		for (Map.Entry<String, String> entry : data.entrySet()) {
			payload.append(entry.getKey() + "=" + entry.getValue() + "&");
		}
		String postData = payload.toString();
		con.setRequestProperty("Content-length", String.valueOf(postData.length()));
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		con.setDoOutput(true);
		con.setDoInput(true);

		DataOutputStream output = new DataOutputStream(con.getOutputStream());
		output.writeBytes(postData);
		output.close();

		int code = con.getResponseCode();
		System.out.println("Response    (Code):" + code);
		System.out.println("Response (Message):" + con.getResponseMessage());

		// read the response
		DataInputStream input = new DataInputStream(con.getInputStream());
		int c;
		StringBuilder resultBuf = new StringBuilder();
		while ((c = input.read()) != -1) {
			resultBuf.append((char) c);
		}
		input.close();

		return resultBuf.toString();
	}

}
