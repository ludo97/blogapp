package com.springboot.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springboot.blog.model.TbUserEntity;

@Repository
public interface TbUserRepository extends JpaRepository<TbUserEntity, Integer> {
	@Query(" SELECT re.userId from TbUserEntity re WHERE re.UUID = (:UUID) ")
	Integer findByUUID(@Param("UUID") String UUID);
}
