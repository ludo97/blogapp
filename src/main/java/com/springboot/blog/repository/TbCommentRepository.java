package com.springboot.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springboot.blog.model.TbCommentEntity;

@Repository
public interface TbCommentRepository extends JpaRepository<TbCommentEntity, Integer> {
	@Query(" SELECT re from TbCommentEntity re WHERE re.articleId = (:articleId)")
	List<TbCommentEntity> findByArticleId(@Param("articleId") Integer articleId);
}