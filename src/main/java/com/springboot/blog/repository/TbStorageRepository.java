package com.springboot.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springboot.blog.model.TbStorageEntity;

@Repository
public interface TbStorageRepository extends JpaRepository<TbStorageEntity, Integer> {
	@Query(" SELECT re.id from TbStorageEntity re WHERE re.fileId = (:fileId) ")
	Integer findByFileId2(@Param("fileId") Integer fileId);
}
