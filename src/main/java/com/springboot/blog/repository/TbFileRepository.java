package com.springboot.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springboot.blog.model.TbFileEntity;

@Repository
public interface TbFileRepository extends JpaRepository<TbFileEntity, Integer> {
	@Query(" SELECT re from TbFileEntity re WHERE ((:tag = '' AND :fileName = '' AND :description = '') OR ((:tag<>'' AND tag like CONCAT('%', :tag, '%')) OR (:fileName <> '' AND fileName like CONCAT('%', :fileName, '%')) OR (:description <> '' AND description like CONCAT('%', :description, '%')))) ")
	List<TbFileEntity> bySearch(@Param("tag") String tag, @Param("description") String description,
			@Param("fileName") String fileName);

}
