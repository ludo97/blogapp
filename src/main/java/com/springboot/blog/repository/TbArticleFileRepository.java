package com.springboot.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springboot.blog.model.TbArticleFileEntity;

@Repository
public interface TbArticleFileRepository extends JpaRepository<TbArticleFileEntity, Integer> {
	@Query(" SELECT re from TbArticleFileEntity re WHERE re.articleId = (:articleId)")
	List<TbArticleFileEntity> findByArticleId(@Param("articleId") Integer articleId);

	@Query(" SELECT re.articleFileId from TbArticleFileEntity re WHERE re.articleId = (:articleId)")
	List<TbArticleFileEntity> findByArticleId2(@Param("articleId") Integer articleId);
}
