package com.springboot.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springboot.blog.model.TbArticleEntity;

@Repository
public interface TbArticleRepository extends JpaRepository<TbArticleEntity, Integer> {
	@Query(" SELECT re from TbArticleEntity re WHERE ((:tag = '' AND :title = '' AND :text = '') OR ((:tag<>'' AND tag like CONCAT('%', :tag, '%')) OR (:title <> '' AND title like CONCAT('%', :title, '%')) OR (:text <> '' AND text like CONCAT('%', :text, '%')))) ")
	List<TbArticleEntity> bySearch(@Param("tag") String tag, @Param("text") String text, @Param("title") String title);
}
