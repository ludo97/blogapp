package com.springboot.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springboot.blog.model.TbCommentFileEntity;

@Repository
public interface TbCommentFileRepository extends JpaRepository<TbCommentFileEntity, Integer> {
	@Query(" SELECT re from TbCommentFileEntity re WHERE re.commentId = (:commentId) ")
	List<TbCommentFileEntity> findByCommentId(@Param("commentId") Integer commentId);

	@Query(" SELECT re.commentFileId from TbCommentFileEntity re WHERE re.commentId = (:commentId) ")
	List<TbCommentFileEntity> findByCommentId2(@Param("commentId") Integer commentId);
}