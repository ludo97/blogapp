package com.springboot.blog.service;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.WebUtils;

import com.springboot.blog.DTO.UserRegDTO;
import com.springboot.blog.exception.RecordNotFoundException;
import com.springboot.blog.model.TbUserEntity;
import com.springboot.blog.repository.TbUserRepository;
import com.springboot.blog.utility.RestClient;

@Service
public class TbUserService {
	@Autowired
	TbUserRepository repository;

	@Autowired
	RestClient rc;

	public TbUserEntity findById(Integer id) throws RecordNotFoundException {
		if (id != null) {
			Optional<TbUserEntity> recipe = repository.findById(id);
			return recipe.get();
		} else {
			return new TbUserEntity();
		}
	}

	public TbUserEntity save(UserRegDTO entity, String UUID) throws RecordNotFoundException {

		TbUserEntity newEntity = new TbUserEntity();
		newEntity.setUsername(entity.getUsername());
		newEntity.setEmail(entity.getEmail());
		newEntity.setUUID(UUID);
		newEntity.setActive(true);
		newEntity.setDateRegistration(new Date());
		repository.save(newEntity);

		return newEntity;
	}

	public Map<String, Object> isLogged(HttpServletRequest request) throws IOException {
		Object serviceStatus = null;
		JSONObject json = null;
		Map<String, Object> content3 = new HashMap<String, Object>();
		if (WebUtils.getCookie(request, "userUUID") != null) {
			String userUUID2 = WebUtils.getCookie(request, "userUUID").getValue();
			String appUUID2 = WebUtils.getCookie(request, "appUUID").getValue();
			String userSessionUUID2 = WebUtils.getCookie(request, "userSessionUUID").getValue();

			Map<String, String> data = new HashMap<String, String>();
			data.put("logAppUUID", appUUID2);
			data.put("logUserUUID", userUUID2);
			data.put("logUserSessionUUID", userSessionUUID2);

			String response = rc.post("http://authrest.dot-env.com/it/userSession/isLogged", data);
			Object obj = JSONValue.parse(response);
			json = (JSONObject) obj;
			serviceStatus = json.get("serviceStatus");
			content3.put("serviceStatus", serviceStatus);
		} else {
			content3.put("non sei loggato", serviceStatus);
		}
		return content3;
	}

	public Map<String, Integer> getByUUID(String UUID) throws IOException {
		Map<String, Integer> content3 = new HashMap<String, Integer>();
		Integer id = 0;
		if (UUID != null) {

			id = repository.findByUUID(UUID);

			content3.put("userId", id);
		} else {
			content3.put("parametri inviati non validi", id);
		}

		return content3;
	}

	public Integer returnUserId(HttpServletRequest request) throws IOException {

		Integer id = null;
		Map<String, Object> sresponse;
		Object ut = request.getSession().getAttribute("utente");
		if(  ut == null ) {
		 sresponse = isLogged(request);
		Object serviceStatus2 = sresponse.get("serviceStatus");
		if((Boolean)serviceStatus2) {
		String userUUID2 = WebUtils.getCookie(request, "userUUID").getValue();
		Map<String, Integer> response2 = getByUUID(userUUID2);
		id = response2.get("userId");
		}
		}else {
			String userUUID2 = WebUtils.getCookie(request, "userUUID").getValue();
			Map<String, Integer> response2 = getByUUID(userUUID2);
			id = response2.get("userId");
		}

		return id;
	}

	public void deleteById(Integer id) throws RecordNotFoundException {
		repository.deleteById(id);
	}
}
