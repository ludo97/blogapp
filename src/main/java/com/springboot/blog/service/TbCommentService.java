package com.springboot.blog.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.blog.exception.RecordNotFoundException;
import com.springboot.blog.model.TbCommentEntity;
import com.springboot.blog.repository.TbCommentRepository;

@Service
public class TbCommentService {
	@Autowired
	TbCommentRepository repository;

	public List<TbCommentEntity> findByArticleId(Integer articleId) throws RecordNotFoundException {
		if (articleId != null) {
			List<TbCommentEntity> comment = repository.findByArticleId(articleId);
			return comment;
		} else {
			return new ArrayList<TbCommentEntity>();
		}
	}

	public TbCommentEntity edit(TbCommentEntity entity) throws RecordNotFoundException {
		if (entity.getCommentId() != null) {
			Optional<TbCommentEntity> comment = repository.findById(entity.getCommentId());
			if (comment.isPresent()) {
				TbCommentEntity newEntity = comment.get();

				newEntity.setCommentId(entity.getCommentId());
				newEntity.setArticleId(entity.getArticleId());
				newEntity.setText(entity.getText());
				newEntity.setActive(entity.getActive());

				newEntity = repository.save(newEntity);
				return newEntity;
			} else {
				entity = repository.save(entity);
				return entity;
			}
		} else {
			entity = repository.save(entity);
			return entity;
		}
	}

	public void deleteById(Integer id) throws RecordNotFoundException {
		repository.deleteById(id);
	}
}
