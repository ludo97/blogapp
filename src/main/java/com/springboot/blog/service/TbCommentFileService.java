package com.springboot.blog.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.blog.exception.RecordNotFoundException;
import com.springboot.blog.model.TbCommentFileEntity;
import com.springboot.blog.repository.TbCommentFileRepository;

@Service
public class TbCommentFileService {
	@Autowired
	TbCommentFileRepository repository;

	public List<TbCommentFileEntity> findByCommentId(Integer commentId) throws RecordNotFoundException {
		if (commentId != null) {
			List<TbCommentFileEntity> commentFile = repository.findByCommentId(commentId);
			return commentFile;
		} else {
			return new ArrayList<TbCommentFileEntity>();
		}
	}

	public List<TbCommentFileEntity> findByCommentId2(Integer commentId) throws RecordNotFoundException {
		if (commentId != null) {
			List<TbCommentFileEntity> commentFile = repository.findByCommentId2(commentId);
			return commentFile;
		} else {
			return new ArrayList<TbCommentFileEntity>();
		}
	}

	public TbCommentFileEntity edit(TbCommentFileEntity entity) throws RecordNotFoundException {
		if (entity.getCommentFileId() != null) {
			Optional<TbCommentFileEntity> commentFile = repository.findById(entity.getCommentFileId());
			if (commentFile.isPresent()) {
				TbCommentFileEntity newEntity = commentFile.get();

				newEntity.setCommentFileId(entity.getCommentFileId());
				newEntity.setCommentId(entity.getCommentId());
				newEntity.setFileId(entity.getFileId());
				newEntity.setActive(entity.getActive());

				newEntity = repository.save(newEntity);
				return newEntity;
			} else {
				entity = repository.save(entity);
				return entity;
			}
		} else {
			entity = repository.save(entity);
			return entity;
		}
	}

	public void deleteById(Integer id) throws RecordNotFoundException {
		repository.deleteById(id);
	}
}