package com.springboot.blog.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.blog.exception.RecordNotFoundException;
import com.springboot.blog.model.TbArticleFileEntity;
import com.springboot.blog.repository.TbArticleFileRepository;

@Service
public class TbArticleFileService {
	@Autowired
	TbArticleFileRepository repository;

	public List<TbArticleFileEntity> findByArticleId(Integer articleId) throws RecordNotFoundException {
		if (articleId != null) {
			List<TbArticleFileEntity> articleFile = repository.findByArticleId(articleId);
			return articleFile;
		} else {
			return new ArrayList<TbArticleFileEntity>();
		}
	}

	public List<TbArticleFileEntity> findByArticleId2(Integer articleId) throws RecordNotFoundException {
		if (articleId != null) {
			List<TbArticleFileEntity> articleFile = repository.findByArticleId2(articleId);
			return articleFile;
		} else {
			return new ArrayList<TbArticleFileEntity>();
		}
	}

	public TbArticleFileEntity edit(TbArticleFileEntity entity) throws RecordNotFoundException {
		if (entity.getArticleFileId() != null) {
			Optional<TbArticleFileEntity> articleFile = repository.findById(entity.getArticleFileId());
			if (articleFile.isPresent()) {
				TbArticleFileEntity newEntity = articleFile.get();

				newEntity.setArticleFileId(entity.getArticleFileId());
				newEntity.setArticleId(entity.getArticleId());
				newEntity.setFileId(entity.getFileId());
				newEntity.setActive(entity.getActive());

				newEntity = repository.save(newEntity);
				return newEntity;
			} else {
				entity = repository.save(entity);
				return entity;
			}
		} else {
			entity = repository.save(entity);
			return entity;
		}
	}

	public void deleteById(Integer id) throws RecordNotFoundException {
		repository.deleteById(id);
	}
}