package com.springboot.blog.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.blog.exception.RecordNotFoundException;
import com.springboot.blog.model.TbArticleEntity;
import com.springboot.blog.repository.TbArticleRepository;

@Service
public class TbArticleService {
	@Autowired
	TbArticleRepository repository;

//	crea repository by search
	public List<TbArticleEntity> findForSearch(String tag, String text, String title) throws RecordNotFoundException {
		List<TbArticleEntity> article = repository.bySearch(tag, text, title);
		return article;

	}

	public TbArticleEntity edit(TbArticleEntity entity) throws RecordNotFoundException {
		if (entity.getArticleId() != null) {
			Optional<TbArticleEntity> article = repository.findById(entity.getArticleId());
			if (article.isPresent()) {
				TbArticleEntity newEntity = article.get();

				newEntity.setArticleId(entity.getArticleId());
				newEntity.setTag(entity.getTag());
				newEntity.setTitle(entity.getTitle());
				newEntity.setText(entity.getText());
				newEntity.setActive(entity.getActive());

				newEntity = repository.save(newEntity);
				return newEntity;
			} else {
				entity = repository.save(entity);
				return entity;
			}
		} else {
			entity = repository.save(entity);
			return entity;
		}
	}

	public void deleteById(Integer id) throws RecordNotFoundException {
		repository.deleteById(id);
	}
}