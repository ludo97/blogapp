package com.springboot.blog.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.blog.exception.RecordNotFoundException;
import com.springboot.blog.model.TbFileEntity;
import com.springboot.blog.model.TbStorageEntity;
import com.springboot.blog.repository.TbFileRepository;
import com.springboot.blog.repository.TbStorageRepository;

@Service
public class TbFileService {
	@Autowired
	TbFileRepository repository;

	@Autowired
	TbStorageRepository repository2;

	public TbFileEntity findById(Integer id) {
		if (id != null) {
			Optional<TbFileEntity> file = repository.findById(id);
			return file.get();
		} else {
			return new TbFileEntity();
		}
	}

	public Integer findByFileId2(Integer fileId) {
		Integer storage = repository2.findByFileId2(fileId);
		return storage;
	}

	public List<TbFileEntity> findForSearch(String tag, String description, String fileName)
			throws RecordNotFoundException {

		List<TbFileEntity> file = repository.bySearch(tag, description, fileName);
		return file;

	}

	public TbFileEntity edit(TbFileEntity entity) throws RecordNotFoundException {
		if (entity.getFileId() != null) {
			Optional<TbFileEntity> file = repository.findById(entity.getFileId());
			if (file.isPresent()) {
				TbFileEntity newEntity = file.get();
				newEntity.setFileId(entity.getFileId());
				newEntity.setFileName(entity.getFileName());
				newEntity.setTag(entity.getTag());
				newEntity.setDescription(entity.getDescription());
				newEntity.setActive(entity.getActive());

				newEntity = repository.save(newEntity);
				return newEntity;
			} else {
				entity = repository.save(entity);
				return entity;
			}
		} else {

			entity = repository.save(entity);
			return entity;
		}
	}

	public TbStorageEntity edit2(TbStorageEntity entity, TbFileEntity updated) throws RecordNotFoundException {

		entity.setFileId(updated.getFileId());

		entity = repository2.save(entity);
		return entity;

	}

	public void deleteById(Integer id) throws RecordNotFoundException {
		repository.deleteById(id);
	}

	public void deleteById2(Integer id) throws RecordNotFoundException {
		repository2.deleteById(id);
	}
}
