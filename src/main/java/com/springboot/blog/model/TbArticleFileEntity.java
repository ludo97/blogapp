package com.springboot.blog.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Cacheable(false)
@Table(name = "tb_article_file")
public class TbArticleFileEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "Integer", name = "articleFileId")
	private Integer articleFileId;
	@Column(columnDefinition = "Integer", name = "articleId")
	@NotNull
	private Integer articleId;
	@Column(columnDefinition = "Integer", name = "fileId")
	@NotNull
	private Integer fileId;
	@Column(columnDefinition = "BIT", name = "active")
	private Boolean active;
	@Column(columnDefinition = "DATETIME", name = "dateCreation")
	@NotNull
	private Date dateCreation;

	public Integer getArticleFileId() {
		return articleFileId;
	}

	public void setArticleFileId(Integer articleFileId) {
		this.articleFileId = articleFileId;
	}

	public Integer getArticleId() {
		return articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}

	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

}