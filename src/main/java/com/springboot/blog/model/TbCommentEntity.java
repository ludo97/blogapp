package com.springboot.blog.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Cacheable(false)
@Table(name = "tb_comment")
public class TbCommentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "Integer", name = "commentId")
	private Integer commentId;
	@Column(columnDefinition = "Integer", name = "articleId")
	@NotNull
	private Integer articleId;
	@Column(columnDefinition = "Integer", name = "userId")
	@NotNull
	private Integer userId;
	@Column(columnDefinition = "TEXT", name = "text")
	@NotNull
	private String text;
	@Column(columnDefinition = "BIT", name = "active")
	private Boolean active;
	@Column(columnDefinition = "DATETIME", name = "dateCreation")
	@NotNull
	private Date dateCreation;

	public Integer getCommentId() {
		return commentId;
	}

	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

	public Integer getArticleId() {
		return articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

}
