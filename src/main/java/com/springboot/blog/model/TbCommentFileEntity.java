package com.springboot.blog.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Cacheable(false)
@Table(name = "tb_comment_file")
public class TbCommentFileEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "Integer", name = "commentFileId")
	private Integer commentFileId;
	@Column(columnDefinition = "Integer", name = "commentId")
	@NotNull
	private Integer commentId;
	@Column(columnDefinition = "Integer", name = "fileId")
	@NotNull
	private Integer fileId;
	@Column(columnDefinition = "BIT", name = "active")
	private Boolean active;
	@Column(columnDefinition = "DATETIME", name = "dateCreation")
	@NotNull
	private Date dateCreation;

	public Integer getCommentFileId() {
		return commentFileId;
	}

	public void setCommentFileId(Integer commentFileId) {
		this.commentFileId = commentFileId;
	}

	public Integer getCommentId() {
		return commentId;
	}

	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

}
